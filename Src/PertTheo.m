if dimension==2
    psi0th=zeros(NGridPoints,NGridPoints);
    psi1st=zeros(NGridPoints,NGridPoints);
    psi0=psi;
    psi0k=fft2(psi0);
    
    psi0th=psi;
    

    figure('Position',[0 0 1300 650])
    for TimeStep=1:NTimeStep
    tInitial=(TimeStep-1)*Dt;
    tFinal=TimeStep*Dt;
%     %     0th order term calculation
%     psi0th=ifft2(exp(-1i*Tkin(Kx,Ky)*(tFinal-tInitial)/hbar).*fft2(psi0th));


    %     1st order correction calculation
    %         free propagate for tFinal-tInitial
    psi1st=ifft2(exp(-1i*Tkin(Kx,Ky)*(tFinal-tInitial)/hbar).*fft2(psi1st));
    
    

    %     add hitting by the potential
    Dt2=0.2*Dt;
    tPrimeList=tInitial:Dt2:tFinal-Dt2;
    for idx=1:numel(tPrimeList)
        tPrime=tPrimeList(idx);
        psi1st=psi1st+1/(1i*hbar)*Dt2*HitOnce(psi0,Kx,Ky,tPrime,tFinal,hbar,Tkin,V);
    end

        PbDistKspace=abs(fft2(psi1st)/sqrt(Dkx*Dky*norm(psi0k(:))^2)).^2;
    %     visualization
    if mod(TimeStep,SavingPeriod)==0 && plotMRK==1    
    psi=psi1st;
    
    
    
    tiledlayout(1,2);
    nexttile;
    PlotWFReal;
    
    nexttile;
    PlotWFReciprocal;
    
    MRK(TimeStep/SavingPeriod)=getframe(gcf);
    disp(['Loop ',num2str(TimeStep),'/',num2str(NTimeStep),': ',num2str(toc),' s'])
    tic
    end
    
    if mod(TimeStep,SavingPeriodAvgK)==0
        avgKx(floor(TimeStep/SavingPeriodAvgK)+1)=Dkx*Dky*sum(PbDistKspace.*Periodick(Kx),'all');
    end
    
    if mod(TimeStep,SavingPeriod2)==0
        for j=1:length(KAngleBins)-1
            PDFAngular(floor(TimeStep/SavingPeriod2)+1,j)=sum(double(KAngleBins(j)<=KAngleGrid & KAngleGrid<KAngleBins(j+1)).*PbDistKspace,'all')*Dkx*Dky;
        end
        if ScattPopAnal==1
            ScatteredPopulationPT(1,floor(TimeStep/SavingPeriod2)+1)=sum(PbDistKspace,'all')*Dkx*Dky;
%             ScatteredPopulationPT(1,floor(TimeStep/SavingPeriod2)+1)=sum(((Kx-kF).^2+Ky.^2>(.2*kF)^2).*PbDistKspace,'all')*Dkx*Dky;
            
%             ScatteredPopulation(1,floor(TimeStep/SavingPeriod2)+1)=sum(double(2*thetaB(1)-.3<=KAngleGrid & KAngleGrid<2*thetaB(1)+.3).*PbDistKspace,'all')*Dkx*Dky;
%                 ScatteredPopulation2(1,floor(TimeStep/SavingPeriod2)+1)=sum(double(-2*thetaB(1)-.3<=KAngleGrid & KAngleGrid<-2*thetaB(1)+.3).*PbDistKspace,'all')*Dkx*Dky;
%                 ScatteredPopulation3(1,floor(TimeStep/SavingPeriod2)+1)=sum(double(-.3<=KAngleGrid & KAngleGrid<.3).*PbDistKspace,'all')*Dkx*Dky;
        end
    end
    end
psi=psi0;
end

% if dimension==2 && recording==1
% %     v=VideoWriter([FigureDir DataLabel '_RKPTmovie.avi']);
%     v=VideoWriter([FigureDir '_RKPTmovie.avi']);
%     v.FrameRate=3;
%     open(v);
%     writeVideo(v,MRK)
%     close(v)
%     
%     %     v2=VideoWriter([FigureDir DataLabel '_KAngleDist.avi']);
%     %     open(v2);
%     %     writeVideo(v2,MTheta)
%     %     close(v2)
% end
