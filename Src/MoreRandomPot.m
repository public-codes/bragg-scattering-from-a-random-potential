function ret = MoreRandomPot(x,y,q,A,NModes,phase)
ang=linspace(0,2*pi,NModes+1);
ang=ang(1:end-1);%exclude duplicated mode at ang=0=2*pi
ret=0.;
for j=1:NModes
    ret=ret+A/sqrt(NModes)*cos((q*(1+.2*cos(2.*(ang(j)-pi/2))))*(x*cos(ang(j))+y*sin(ang(j)))+phase(j));
end
end