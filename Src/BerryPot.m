function ret = BerryPot(x,y,q,A,NModes,phase)
ang=linspace(0,2*pi,NModes+1);
ang=ang(1:end-1);%exclude duplicated mode at ang=0=2*pi
ret=0.;
for j=1:NModes
%     ret=ret+A/sqrt(NModes)*cos(1.5*q*cos(1.414*ang(j))*(x*cos(ang(j))+y*sin(ang(j)))+phase(j));
    ret=ret+A/sqrt(NModes)*cos(q*(x*cos(ang(j))+y*sin(ang(j)))+phase(j));
end
end