if mod(TimeStep,SavingPeriodAvgK)==0
    psik=fft2(psi);
    PbDistRspace=abs(psi/sqrt(Dx*Dy*norm(psi(:))^2)).^2;
    PbDistKspace=abs(psik/sqrt(Dkx*Dky*norm(psik(:))^2)).^2;
    
    %     avgKx(floor(TimeStep/SavingPeriodAvgK)+1)=Dkx*Dky*sum(PbDistKspace.*Periodick(Kx),'all')-Dx*Dy*sum(PbDistRspace.*Y,'all')*e*B/hbar;
    %     avgKy(floor(TimeStep/SavingPeriodAvgK)+1)=Dkx*Dky*sum(PbDistKspace.*Periodick(Ky),'all');
    %     MSDistK(floor(TimeStep/SavingPeriodAvgK)+1)=Dkx*Dky*sum(PbDistKspace.*(Periodick(Kx).^2+Periodick(Ky).^2),'all');
%     avgX(floor(TimeStep/SavingPeriodAvgK)+1)=Dx*Dy*sum(PbDistRspace.*X,'all');
    %     avgY(floor(TimeStep/SavingPeriodAvgK)+1)=Dx*Dy*sum(PbDistRspace.*Y,'all');
%     MSDist(floor(TimeStep/SavingPeriodAvgK)+1)=Dx*Dy*sum(PbDistRspace.*(X.^2+Y.^2),'all');
    
    %     for j=1:length(KAngleBins)-1
    %         PDFAngular(floor(TimeStep/SavingPeriod2)+1,j)=sum(double(KAngleBins(j)<=KAngleGrid & KAngleGrid<KAngleBins(j+1)).*PbDistKspace,'all')*Dkx*Dky;
    %     end
    
    if ScattPopAnal==1
        %         ScatteredPopulation(1,floor(TimeStep/SavingPeriod2)+1)=sum(((Kx-kF*cos(2*thetaB(1))).^2+(Ky-kF*sin(2*thetaB(1))).^2<=(.3*kF)^2).*PbDistKspace,'all')*Dkx*Dky;
        ScatteredPopulation(1,floor(TimeStep/SavingPeriod2)+1)=sum(((Kx-kF).^2+Ky.^2>(.2*kF)^2).*PbDistKspace,'all')*Dkx*Dky;
        
        %         ScatteredPopulation(1,floor(TimeStep/SavingPeriod2)+1)=sum(double(2*thetaB(1)-.3<=KAngleGrid & KAngleGrid<2*thetaB(1)+.3).*PbDistKspace,'all')*Dkx*Dky;
        %         ScatteredPopulation2(1,floor(TimeStep/SavingPeriod2)+1)=sum(double(-2*thetaB(1)-.3<=KAngleGrid & KAngleGrid<-2*thetaB(1)+.3).*PbDistKspace,'all')*Dkx*Dky;
        %         ScatteredPopulation3(1,floor(TimeStep/SavingPeriod2)+1)=sum(double(-.3<=KAngleGrid & KAngleGrid<.3).*PbDistKspace,'all')*Dkx*Dky;
        %             ScatteredPopulation(1,floor(TimeStep/SavingPeriod2)+1)=sum(double(KAngleBins((NBins+1)/2+1)<=KAngleGrid & KAngleGrid<KAngleBins((NBins+1)/2+1)+1.).*PbDistKspace,'all')*Dkx*Dky;
        %             ScatteredPopulation2(1,floor(TimeStep/SavingPeriod2)+1)=sum(double(KAngleBins((NBins+1)/2)-1.<=KAngleGrid & KAngleGrid<KAngleBins((NBins+1)/2)).*PbDistKspace,'all')*Dkx*Dky;
        %             ScatteredPopulation3(1,floor(TimeStep/SavingPeriod2)+1)=sum(double(KAngleBins((NBins+1)/2)<=KAngleGrid & KAngleGrid<KAngleBins((NBins+1)/2+1)).*PbDistKspace,'all')*Dkx*Dky;
    end
end





%         Plot every SavingPeriod
if mod(TimeStep,SavingPeriod)==0 && plotMRK==1
        tiledlayout(1,2);
    
    
    
    nexttile;
    PlotWFReal;
        
    nexttile;
    PlotWFReciprocal;
    
    
    
    
    %     nexttile;
    %     PbDistKspacetemp=PbDistKspace;
    %     PbDistKspace=PbDistKspace2;
    %     PlotWFReciprocal;
    %     PbDistKspace=PbDistKspacetemp;
    
    
    %     nexttile;
    %     plot3(KxShifted,KyShifted,VkSq.*(sinc(hbar^2*q.^2/(2*m)*Dt*TimeStep/(2*hbar))*Dt*TimeStep).^2)
    %     xlim([-1 1]*min(1.5*qD,KxShifted(1,NGridPoints)))
    %     ylim([-1 1]*min(1.5*qD,KxShifted(1,NGridPoints)))
    %     pbaspect([1 1 1])
    %     hold on
    %     % % Nest circle
    %     % ax2=axes('Position',ax1.Position);
    %     % for plotting circle
    %     ang=0:pi/50:2*pi;
    %     rc=sqrt(2*m*2*pi/hbar/(TimeStep*Dt)); % For nesting
    %     xc=rc*cos(ang);%circle coordinates
    %     yc=rc*sin(ang);
    %
    %     plot(xc,yc,':m','LineWidth',1.5)
    %     hold off
    
    
        tt=0:SavingPeriodAvgK:NTimeStep;
        time=tt*Dt;
        
        %         nexttile;
        %         plot(time,avgKx)
        %
        %         FitMin=1;
        %         FitMax=min([floor(TimeStep/SavingPeriodAvgK) round(tauQPT/Dt/SavingPeriodAvgK)]);
        %         [linearfitk,ErrorStruc]=polyfit(time(FitMin:FitMax),log(avgKx(FitMin:FitMax)),1);%fitting upto FitMax-th savedtimestep
        %         tauk=-1/linearfitk(1);
        %         intercept=linearfitk(2);
        %         expfnk=avgKx(1)*exp(-time/tauk);
        %
        %
        %         hold on
        %         plot(time,expfnk)
        %         plot(time,avgKx(1)*exp(-time/tauQPT))
        %         %     plot(time,avgKx(1)*exp(-time.^2/(2*tauQPT^2)))
        %         hold off
        %         xlim([0 max([TimeStep,1])*Dt])
        %         title(['normr=',num2str(ErrorStruc.normr/ErrorStruc.df*1e5)])
        
        
        
        %         nexttile;
        %         plot(KAngleBinsCenter,PDFAngular(floor(TimeStep/SavingPeriod2)+1,:)/Binsize,'LineWidth',2)
        %         hold on
        %         %         circular standard deviation wrapped normal distribution
        %         circularstdev=sqrt(2*TimeStep*Dt/tauQPT);
        %         %         WNf=1/sqrt(2*pi*circularstdev^2)*exp(-KAngleBinsCenter.^2/(2*circularstdev^2));
        %         rho=exp(-circularstdev*circularstdev/2);
        %         for i=1:length(KAngleBinsCenter)
        %             WNf(i)=JacobiTheta3((KAngleBinsCenter(i)-0)/2,rho)/(2*pi);
        %         end
        %         plot(KAngleBinsCenter,WNf,'LineWidth',2)
        %         hold off
        %         set(gca,'FontSize',FontSizeOverall)
        %         title(['Angular probability distribution',newline,'timestep=',num2str(TimeStep),'/',num2str(NTimeStep)],'FontSize',FontSizeAxes)
        %         xlabel('Scattering angle, 2\theta','FontSize',FontSizeAxes)
        %         ylabel('Probability','FontSize',FontSizeAxes)
        
        
        
        
        
        %         nexttile;
        %         plot(time,MSDistK)
        %         xlim([0 max([TimeStep,1])*Dt])
        %         %         ylim([0 max(MSDistK)*1.2])
        %         xlabel('t')
        %         ylabel('MSDK')
        
        
        MTheta(floor(TimeStep/SavingPeriod2)+1)=getframe(gcf);
    
    
    
    %             nexttile;
    
    %             imagesc([-1 1]*KxShifted(1,NGridPoints),[-1 1]*KxShifted(1,NGridPoints),Pop)
    %             load('MyColormap.mat')
    %             colormap(mymap)
    %             colorbar
    
    
    MRK(TimeStep/SavingPeriod+1)=getframe(gcf);
    %             tLoopEnd=cputime-tLoopStart-tLoopEnd;
    disp(['Loop ',num2str(TimeStep),'/',num2str(NTimeStep),': ',num2str(toc),' s'])
    tic
end


