%     V=zeros(NGridPoints,NGridPoints);
if loadPotential==1
    load(['ModelPot'],'V')
end

if choosePotential==2
    % Berry potential
    potangle=0;
%     phase=rand(1,NModes)*2*pi;
%     phase=zeros(1,NModes);
%     save('MyPhaseSet','phase');
    load('MyPhaseSet.mat');
    V=BerryPot(X,Y,q,A,NModes,phase);
end

if choosePotential==5
    % Berry potential
    potangle=0;
%     phase=rand(1,NModes)*2*pi;
%     phase=zeros(1,NModes);
%     save('MyPhaseSet','phase');
    load('MyPhaseSet.mat');
    V=MoreRandomPot(X,Y,q,A,NModes,phase);
end

if choosePotential==6
    % Berry potential
    potangle=0;
%     phase=rand(1,NModes)*2*pi;
%     phase=zeros(1,NModes);
%     save('MyPhaseSet','phase');
    load('MyPhaseSet.mat');
    V=MoreRandomPot(X,Y,q,A,NModes,phase);
    V=V+MoreRandomPot(X,Y,0.96*q,A,NModes,phase);
    V=V+MoreRandomPot(X,Y,0.98*q,A,NModes,phase);
    V=V+MoreRandomPot(X,Y,1.02*q,A,NModes,phase);
    V=V+MoreRandomPot(X,Y,1.04*q,A,NModes,phase);
    V=V/5;
end


if NeedFlatPart==1
         V=V.*sigmoid(X,-XSize/2*.4,XSize/2*.05);
end

%         save(['ModelPot'],'V')




function val=sigmoid(x,x0,width)
val=1./(1+exp(-(x-x0)/width));
end