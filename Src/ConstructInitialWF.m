%%
psi=zeros(NGridPoints,NGridPoints);

%% % %     single wavepacket
x0=-XSize/2*.55;
y0=0*L;
sigmax=XSize*.045;
sigmay=XSize*.045;

if BroadWavepacket==1
x0=0;
y0=0*L;
sigmax=XSize*.1;
sigmay=XSize*.1;
end

Normalization=(1/(pi*2*sigmax^2)/(pi*2*sigmay^2))^(1/4);

k0theta=0;
k0vec=kF*[cos(k0theta) sin(k0theta)];
% % Gaussian
psi=Normalization*exp(-(X-x0).^2/(4*sigmax^2)-(Y-y0).^2/(4*sigmay^2)).*exp(1i*((k0vec(1))*(X-x0)+k0vec(2)*(Y-y0)));
% % Plane wave
% psi=exp(1i*((k0vec(1)+e*B*y0/hbar)*(X-x0)+k0vec(2)*(Y-y0)));
% % Constant wavefunction
% psi=ones(NGridPoints,NGridPoints);


% % Normalization
psi=psi/sqrt(Dx*Dy*norm(psi(:))^2);


%%
psi0=psi;
