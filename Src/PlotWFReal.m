imagesc([X(1) X(end)],[Y(1) Y(end)],real(psi));
caxis([-1 1]*.8*max(abs(psi),[],'all'))%.15
set(gca,'YDir','normal')
pbaspect([1 1 1])
cmap;
colormap(uu)
% colorbar
set(gca,'FontSize',FontSizeOverall)
set(gca,'XTickLabel',[])
set(gca,'XTick',[])
set(gca,'YTickLabel',[])
set(gca,'YTick',[])
title(['Real part of wavefunction in real space',newline,'TimeStep=',num2str(TimeStep),'/',num2str(NTimeStep)],'FontSize',FontSizeAxes)
% xlabel('x/m','FontSize',FontSizeAxes)
% ylabel('y/m','FontSize',FontSizeAxes)