function r = modOffset(a,b,c)% a can be a vector
tmp=mod(a,b);
if c>=0
    tmp=tmp+b*(tmp<c);
else % c<0
    tmp=tmp-b*(b+c<=tmp);
end
r=tmp;
end