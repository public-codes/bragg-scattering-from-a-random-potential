function psi1st=HitOnce(psi,Kx,Ky,tPrime,tFinal,hbar,Tkin,V)
%     free propagate for time tPrime, and hit potential
psi=V.*ifft2(exp(-1i*Tkin(Kx,Ky)*tPrime/hbar).*fft2(psi));

% free propagate for time (tFinal-tPrime)
psi1st=ifft2(exp(-1i*Tkin(Kx,Ky)*(tFinal-tPrime)/hbar).*fft2(psi));
end