% % reciprocal space distribution
ax1=gca;

imagesc([KxShifted(1,1) KxShifted(1,NGridPoints)],[KxShifted(1,1) KxShifted(1,NGridPoints)],circshift(PbDistKspace,[NGridPoints/2 NGridPoints/2]))

load('MyColormap.mat')
colormap(ax1,mymap)
set(gca,'YDir','normal')
% colorbar
set(gca,'FontSize',FontSizeOverall)
set(gca,'XTickLabel',[])
set(gca,'XTick',[])
set(gca,'YTickLabel',[])
set(gca,'YTick',[])
% title(['Probability distribution in reciprocal space',newline,'timestep=',num2str(TimeStep),'/',num2str(NTimeStep)],'FontSize',FontSizeAxes)

%     title(['T/T_D=',num2str(TRatio),',2k_F/q_D=',num2str(kFToqDover2),',V_{rms}/E_F=',num2str(Vrms/EF,'%.3f')],'FontSize',FontSizeAxes)
% xlim([-1 1]*min(3*kF,KxShifted(1,NGridPoints)))
% ylim([-1 1]*min(3*kF,KxShifted(1,NGridPoints)))



xlim( ([-1 1]*(kF+q)+q/(kF+q)*k0vec(1)) ) % 2*(kF+q) is the size of the window, and shift it to center of mass q/(kF+q)*k0vec
ylim( ([-1 1]*(kF+q)+q/(kF+q)*k0vec(2)) )

pbaspect([1 1 1])
% caxis([0 max(abs(PbDistKspace),[],'all')])
% xlabel('k_x/m^{-1}','FontSize',FontSizeAxes)
% ylabel('k_y/m^{-1}','FontSize',FontSizeAxes)


if choosePotential==2
    % caxis([0 max(abs(PbDistKspace),[],'all')*0.005])
    caxis([0 max(abs(PbDistKspace),[],'all')*.1])
end

if choosePotential==5 || choosePotential==6
    % caxis([0 max(abs(PbDistKspace),[],'all')*0.005])
    caxis([0 max(abs(PbDistKspace),[],'all')*.1])
end


% ax2=ax1;
hold on
% % electron circle
% ax2=axes('Position',ax1.Position);
% for plotting circle
ang=0:pi/50:2*pi;
rc=kF;%radius of circle
xc=rc*cos(ang);%circle coordinates
yc=rc*sin(ang);

plot(xc,yc,':k','LineWidth',1.5)
% pbaspect([1 1 1])
% set(ax2,'XLim',ax1.XLim,'YLim',ax1.YLim,'Visible','off');



% to plot shaded area
DkF=1/sigmax*1.2;
rc=kF-DkF;%radius of circle
xc1=rc*cos(ang);%circle coordinates
yc1=rc*sin(ang);

rc=kF+DkF*1.15;%radius of circle
xc2=rc*cos(ang);%circle coordinates
yc2=rc*sin(ang);

fill([xc1 flip(xc2)],[yc1 flip(yc2)],'k','FaceAlpha',.15,'EdgeAlpha',0)

% % for jjj=1.1:.1:3
% % rc=jjj*kF;%radius of circle
% % xc=rc*cos(ang);%circle coordinates
% % yc=rc*sin(ang);
% % plot(xc,yc,':k')
% % end

% % (band) Edges of reciprocal space
%
%
% ax22=axes('Position',ax1.Position);
rectangle('Position',[-1 -1 2 2]*KxShifted(1,NGridPoints),'EdgeColor','r')
% pbaspect([1 1 1])
% set(ax22,'XLim',ax1.XLim,'YLim',ax1.YLim,'Visible','off');
%


if choosePotential==2
    % % phonon circle for Berry potential
    %
    % for plotting circle
    ang=0:pi/50:2*pi;
    rc=q;%radius of circle
    xc=rc*cos(ang);%circle coordinates
    yc=rc*sin(ang);
    
    plot(xc+k0vec(1),yc+k0vec(2),':m','LineWidth',1.5)
    pbaspect([1 1 1])
    
    
    DkF=1/sigmax*1.5;
    rc=q-DkF;%radius of circle
    xc1=k0vec(1)+rc*cos(ang);%circle coordinates
    yc1=k0vec(2)+rc*sin(ang);
    
    rc=q+DkF*1.;%radius of circle
    xc2=k0vec(1)+rc*cos(ang);%circle coordinates
    yc2=k0vec(2)+rc*sin(ang);
    
    fill([xc1 flip(xc2)],[yc1 flip(yc2)],'m','FaceAlpha',.15,'EdgeAlpha',0)
    
    
    
end

if choosePotential==5
    % % phonon lines for crazy potential
    %
    % for plotting circle
    Modul=@(theta) q*(1+.2*cos(2.*(theta-pi/2)));
    xxx=@(theta) Modul(theta)*cos(theta)+kF;
    yyy=@(theta) Modul(theta)*sin(theta);
    fplot(xxx,yyy,[0 2*pi],':m','LineWidth',1.5)
    pbaspect([1 1 1])
    xxx=@(theta) -Modul(theta)*cos(theta)+kF;
    yyy=@(theta) -Modul(theta)*sin(theta);
    fplot(xxx,yyy,[0 2*pi],':m','LineWidth',1.5)
    pbaspect([1 1 1])    
end

if choosePotential==6
    % % phonon lines for crazy potential
    %
    % for plotting circle
    Modul=@(theta) q*(1+.2*cos(2.*(theta-pi/2)));
    xxx=@(theta) Modul(theta)*cos(theta)+kF;
    yyy=@(theta) Modul(theta)*sin(theta);
    fplot(xxx,yyy,[0 2*pi],':m','LineWidth',1.5)
    pbaspect([1 1 1])
    xxx=@(theta) -Modul(theta)*cos(theta)+kF;
    yyy=@(theta) -Modul(theta)*sin(theta);
    fplot(xxx,yyy,[0 2*pi],':m','LineWidth',1.5)
    pbaspect([1 1 1])    
    
    
    % for plotting circle
    Modul=@(theta) .96*q*(1+.2*cos(2.*(theta-pi/2)));
    xxx=@(theta) Modul(theta)*cos(theta)+kF;
    yyy=@(theta) Modul(theta)*sin(theta);
    fplot(xxx,yyy,[0 2*pi],':m','LineWidth',1.5)
    pbaspect([1 1 1])
    xxx=@(theta) -Modul(theta)*cos(theta)+kF;
    yyy=@(theta) -Modul(theta)*sin(theta);
    fplot(xxx,yyy,[0 2*pi],':m','LineWidth',1.5)
    pbaspect([1 1 1])  
    
    % for plotting circle
    Modul=@(theta) .98*q*(1+.2*cos(2.*(theta-pi/2)));
    xxx=@(theta) Modul(theta)*cos(theta)+kF;
    yyy=@(theta) Modul(theta)*sin(theta);
    fplot(xxx,yyy,[0 2*pi],':m','LineWidth',1.5)
    pbaspect([1 1 1])
    xxx=@(theta) -Modul(theta)*cos(theta)+kF;
    yyy=@(theta) -Modul(theta)*sin(theta);
    fplot(xxx,yyy,[0 2*pi],':m','LineWidth',1.5)
    pbaspect([1 1 1])  
    
    % for plotting circle
    Modul=@(theta) 1.02*q*(1+.2*cos(2.*(theta-pi/2)));
    xxx=@(theta) Modul(theta)*cos(theta)+kF;
    yyy=@(theta) Modul(theta)*sin(theta);
    fplot(xxx,yyy,[0 2*pi],':m','LineWidth',1.5)
    pbaspect([1 1 1])
    xxx=@(theta) -Modul(theta)*cos(theta)+kF;
    yyy=@(theta) -Modul(theta)*sin(theta);
    fplot(xxx,yyy,[0 2*pi],':m','LineWidth',1.5)
    pbaspect([1 1 1])  
    
    % for plotting circle
    Modul=@(theta) 1.04*q*(1+.2*cos(2.*(theta-pi/2)));
    xxx=@(theta) Modul(theta)*cos(theta)+kF;
    yyy=@(theta) Modul(theta)*sin(theta);
    fplot(xxx,yyy,[0 2*pi],':m','LineWidth',1.5)
    pbaspect([1 1 1])
    xxx=@(theta) -Modul(theta)*cos(theta)+kF;
    yyy=@(theta) -Modul(theta)*sin(theta);
    fplot(xxx,yyy,[0 2*pi],':m','LineWidth',1.5)
    pbaspect([1 1 1])  
end


hold off
