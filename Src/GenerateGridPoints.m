x=linspace(-XSize/2,XSize/2,NGridPoints);
y=x;
% z=x;
if dimension==2
    [X,Y]=meshgrid(x,y);
end
if dimension==3
    [X,Y,Z]=meshgrid(x,y,z);
end
Dx=XSize/(NGridPoints-1);
Dy=Dx;
KxSize=2*pi/Dx;
KySize=KxSize;
Dkx=2*pi/XSize;
Dky=Dkx;


l=[0:NGridPoints/2-1,-NGridPoints/2:-1];
% l=l+.5; % Monkhorst-Pack grid
Periodicl=@(l1) modOffset(l1,NGridPoints,-NGridPoints/2);
k=l*Dkx;
if dimension==2
    [Kx,Ky]=meshgrid(k);
end
Periodick=@(k1) Periodicl(k1/Dkx)*Dkx;

KxShifted=circshift(Kx,[NGridPoints/2 NGridPoints/2]);
KyShifted=circshift(Ky,[NGridPoints/2 NGridPoints/2]);


KAngleGrid=atan2(Ky,Kx);
NBins=181;
Binsize=2*pi/NBins;%/41
KAngleBins=-pi:Binsize:pi;
KAngleBinsCenter=KAngleBins(1:end-1)+Binsize/2;
PDFAngular=zeros(floor(NTimeStep/SavingPeriod2)+1,length(KAngleBins)-1);
if ScattPopAnal==1
    ScatteredPopulation=zeros(1,floor(NTimeStep/SavingPeriod2)+1);
    ScatteredPopulation2=zeros(1,floor(NTimeStep/SavingPeriod2)+1);
    ScatteredPopulationPT=zeros(1,floor(NTimeStep/SavingPeriod2));
    ScatteredPopulation2PT=zeros(1,floor(NTimeStep/SavingPeriod2));
end


% Determining font size depending on OS
if ismac==1
    FontSizeOverall=18;
    FontSizeAxes=24;
else
    if isunix==1
        FontSizeOverall=12;
        FontSizeAxes=16;
    else
        FontSizeOverall=14;
        FontSizeAxes=18;
    end
end