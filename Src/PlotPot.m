%%
figure('Position',[000 000 600 600])
%     ax111=axes();
%     contour(X,Y,V,'Fill','on')
imagesc([X(1) X(end)],[Y(1) Y(end)],V);
load('RWBcmap.mat')
% colormap(flipud(gray))
pbaspect([1 1 1])
colormap(gray)
set(gca,'YDir','normal')
% colorbar
caxis([-1 1]*1.1*max(abs(V),[],'all'));
set(gca,'FontSize',FontSizeOverall)
set(gca,'XTickLabel',[])
set(gca,'XTick',[])
set(gca,'YTickLabel',[])
set(gca,'YTick',[])
% title(['Potential',newline,'(real space)'],'FontSize',FontSizeAxes)
% xlabel('x/m','FontSize',FontSizeAxes)
% ylabel('y/m','FontSize',FontSizeAxes)
%         hold on
%         %     to indicate wave packet upto 2*sigma
%         %     ax222=axes('Position',ax111.Position);
%         % for plotting ellipse
%         ang=0:pi/50:2*pi;
%         xc=x0+3*sigmax*cos(ang);%circle coordinates
%         yc=y0+3*sigmay*sin(ang);
%
%         plot(xc,yc,':k')
%         % pbaspect([1 1 1])
%         % set(ax222,'XLim',ax111.XLim,'YLim',ax111.YLim,'Visible','off');
%         hold off
if choosePotential==2
    hold on
    %plot([X(1,floor(NGridPoints*.8)) X(1,floor(NGridPoints*.8))+3.8317/qphonon],[Y(floor(NGridPoints*.1),1) Y(floor(NGridPoints*.1),1)],'LineWidth',5)
    %plot([X(1,floor(NGridPoints*.8)) X(1,floor(NGridPoints*.8))+7.0156/qphonon],[Y(floor(NGridPoints*.09),1) Y(floor(NGridPoints*.09),1)],'LineWidth',5)
    hold off
end

if choosePotential==1
hold on
rectangle('Position',[-1 -1 2 2]*KxShifted(1,NGridPoints),'EdgeColor','r')
hold off
end


%%
% Plotting Fourier transform of potential
% figure;
figure('Position',[600 000 600 600])
VkSq=circshift(abs(fft2(V)).^2,[NGridPoints/2 NGridPoints/2]);
imagesc([-1 1]*KxShifted(1,NGridPoints),[-1 1]*KxShifted(1,NGridPoints),VkSq)
load('RWBcmap.mat')
colormap(flipud(gray))
set(gca,'YDir','normal')
set(gca,'FontSize',FontSizeOverall)
colorbar
%        caxis([-1 1]*1.1*max(abs(V),[],'all'));
title(['Potential',newline,'(reciprocal space)'],'FontSize',FontSizeAxes)
xlabel('k_x/m^{-1}','FontSize',FontSizeAxes)
ylabel('k_y/m^{-1}','FontSize',FontSizeAxes)

if choosePotential==1
hold on
rectangle('Position',[-1 -1 2 2]*qx(end),'EdgeColor','r')
hold off
end