if ScattPopAnal==1
    % plot the population growth of certain angle
    figure;
    tt=0:SavingPeriod2:NTimeStep;
    time=tt*Dt;
    ScatteredPopulation=ScatteredPopulation-ScatteredPopulation(1);%remove integration of initial wavefunction
    
    plot(time,ScatteredPopulation,'LineWidth',2,'DisplayName','Simulation')
    set(gca,'FontSize',FontSizeOverall)
    title(['Population growth of',newline,'scattered waves'],'FontSize',FontSizeAxes)
    xlabel('t/s','FontSize',FontSizeAxes)
    ylabel('Population','FontSize',FontSizeAxes)
    
    
    
    hold on    
    if TDPertTheo==1
        plot(time,ScatteredPopulationPT,'LineWidth',2,'DisplayName','Perturbation Theory')
    end
    if choosePotential==3
        plot(time,A^2/hbar^2*time.^2/4,'LineWidth',2)
        plot(time,sin(A/2/hbar*time).^2)
        omega21=hbar^2*((2*sqrt(3)*kF)^2-kF^2)/(2*m)/hbar;
        RabiFreq=sqrt((A/2/hbar)^2+(omega21/2)^2);
        plot(time,(A/2/hbar)^2/RabiFreq^2*sin(RabiFreq*time).^2)
    end
    hold off
    legend('Location','northwest','FontSize',FontSizeAxes)
    print([FigureDir DataLabel '_PopulationGrowth'],'-dpng')
end



% if ScattPopAnal==1
% %    save([FigureDir DataLabel '_SomeVars'],'avgKx','SavingPeriodAvgK','PDFAngular','ScatteredPopulation','ScatteredPopulation2','ScatteredPopulation3','tauk','tauQPT')
% save([FigureDir DataLabel '_SomeVars'],'tauk','tauQPT')
% else
% %    save([FigureDir DataLabel '_SomeVars'],'avgKx','SavingPeriodAvgK','PDFAngular','tauk','tauQPT','Vrms','EF')
% save([FigureDir DataLabel '_SomeVars'],'tauk','tauQPT','Vrms','EF')
% end




% if dimension==2 && recording==1
%     v=VideoWriter([FigureDir DataLabel '_RKmovie.avi']);
%     v.FrameRate=1;
%     open(v);
%     writeVideo(v,MRK)
%     close(v)
%     
%     %     v2=VideoWriter([FigureDir DataLabel '_KAngleDist.avi']);
%     %     open(v2);
%     %     writeVideo(v2,MTheta)
%     %     close(v2)
% end

