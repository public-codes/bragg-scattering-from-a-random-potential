% % Split operator code written by Donghwan Kim
% % written in MATLAB R2020a
% % Note the code works for dimension==2.
addpath('Src')
addpath('Src/elfun18');
tMainStart=cputime;
% tMainStart=tic;




GenerateGridPoints;

tic
if dimension==2
    ConstructPotentials;
end


%Vsq=mean(V(:).^2);
%Vrms=sqrt(Vsq);%This is an analytic estimate of the rms of the potential. In our case, the average value of the potential is zero, so rms value will be std(V(:)).
%A/sqrt(2);

if AutoCorrPotNeeded==1
    AutoCorrPot;
end

ConstructInitialWF;

% % % % plotting contour is a little bit slow
if dimension==2
    PlotPot;
end

% return;

if not(isfolder(FigureDir))
    mkdir(FigureDir)
end

% print([FigureDir DataLabel '_Pot'],'-dpng')


% % Generating a flat part of the potential
% % V=V.*smoothconnect(X,-XSize*.1,XSize*.03);
% V=V.*takesmoothcircle(X,Y,0.15*XSize/5,0.02*XSize/5);
% % V=V.*takesmoothcircle(X,Y,12.5*lambda0,1.5*lambda0);
% % save(['ModelPot'],'V')
%
% % % % % plotting contour is a little bit slow
% % if dimension==2
% %     PlotPot;
% % end
% % print(['ModelPot'],'-dpng')


if choosePotential==2
    Vsq=mean(V(:).^2);
    Vrms=sqrt(Vsq);
end






%%
if order==1
    expV=zeros(NGridPoints,NGridPoints);
    expV=exp(-1i*V*Dt/hbar);
end
if order==2
    expVover2=zeros(NGridPoints,NGridPoints);
    expVover2=exp(-1i*V/2*Dt/hbar);
end


Tx=@(k1,k2) hbar^2*Periodick(k1).^2/(2*m);
Ty=@(k1,k2) hbar^2*Periodick(k2).^2/(2*m);
Tkin=@(k1,k2) hbar^2*(Periodick(k1).^2+Periodick(k2).^2)/(2*m);
expT=zeros(NGridPoints,NGridPoints);
expT=exp(-1i*Tkin(Kx,Ky)*Dt/hbar);


AutoCorrWF=zeros(1,NTimeStep+1);
avgKx=zeros(1,floor(NTimeStep/SavingPeriodAvgK)+1);
avgKy=zeros(1,floor(NTimeStep/SavingPeriodAvgK)+1);
avgX=zeros(1,floor(NTimeStep/SavingPeriodAvgK)+1);
avgY=zeros(1,floor(NTimeStep/SavingPeriodAvgK)+1);
MSDist=zeros(1,floor(NTimeStep/SavingPeriodAvgK)+1);
MSDistK=zeros(1,floor(NTimeStep/SavingPeriodAvgK)+1);
IPR=zeros(1,floor(NTimeStep/SavingPeriodAvgK)+1);
JxTotal=zeros(1,floor(NTimeStep/SavingPeriodAvgK)+1);
JyTotal=zeros(1,floor(NTimeStep/SavingPeriodAvgK)+1);
NormTracker=zeros(1,floor(NTimeStep/SavingPeriodAvgK)+1);


if dimension==2
    psik=zeros(NGridPoints,NGridPoints);
end


% TDPertTheo=0;
if TDPertTheo==1
    % % perturbation theory analysis
    PertTheo;
end


figure('Position',[0 0 1300 650])

TimeStep=0;
PlotWFandStoreData;

for TimeStep=1:NTimeStep
    TimePropagator;
    PlotWFandStoreData;
    
    clear psik;
end




tMainEnd=cputime-tMainStart