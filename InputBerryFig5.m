% clear; close all;
% //fundamental constants (not likely to adjust)
hbar = 1.0545718e-34;
kB = 1.38064854e-23;
e = 1.60217662e-19;

%     //material specific constants (likely to adjust)
% the following is mostly from 3D copper to make mock 2D copper
me = 9.10938356e-31;
m=me*1;

EF=2.00*e;
vF = sqrt(2*EF/me); %group velocity. me*vF=hbar*kF
kF = me*vF/hbar;

NGridPoints=512*2; %Assumed to be an even number
HbarRatio=1;

hbar0=hbar;
lambda0=2*pi/kF;
hbar=hbar0*HbarRatio;
kF=kF/HbarRatio;%Fix momentum

q=kF*sqrt(2)*HbarRatio;

%     //simulation specific parameters (likely to adjust)
ScattPopAnal=1;

Dt=.5e-16;


choosePotential=2;%2 for Berry, 5 for more general potential
loadPotential=0;


plotMRK=1;



AutoCorrPotNeeded=0;



L=NGridPoints*pi/q/8;







dimension=2;
recording=1;
order=2;%order=1 for expT.*expV, order=2 for expVover2.*expT.*expVover2
NTimeStep=10000;

SavingPeriod=50;
SavingPeriod2=1;
SavingPeriodAvgK=1;


NeedFlatPart=0;
TDPertTheo=0;
BroadWavepacket=1;


XSize=1*L;
YSize=XSize;


NModes=256*2;

A=EF*.06*sqrt(2); % to make potential fluctuation moderate (6%)


FigureDir='data_figures/';

if choosePotential==2
%     DataLabel=sprintf('Dt%dHbR%d',round(Dt/1e-17),round(HbarRatio*100));
    DataLabel='Berry';
end

JobName=DataLabel;